'''
Created on Nov 25, 2014

@author: mladen
'''
from PySide.QtGui import QMenuBar, QAction, QIcon


class Menu(QMenuBar):
    """Klasa koja olaksava kreiranje menija. Ima nekoliko odabranih funkcija za dodavanje podmenija.
    Pri inicijalizaciji se dodaju osnovni meniji poput file, edit, format itd.
    Args:    
        parent: objekat na koji se meni zakaci, odnosno roditelj.
    """
    def __init__(self,parent):
        QMenuBar.__init__(self)
        self.__parent=parent
        parent.fileMenu=self.add('&File')
        self.editMenu=self.add('&Edit')
        self.formatMenu=self.add('F&ormat')
        self.helpMenu=self.add('&Help')
    """Funkcija add, dodaje novi meni u paletu menija.
    Args:
        name:(string) ime menija koji se dodaje.
        label:(string) tekst, odnosno label koji ce biti ispisan na dodatom meniju.
    """
    def add(self,label):
        self.addMenu(label)
    """Funkcija submenu dodaje novi podmeni, za odabrani meni.
    Args:
        menu:meni na koji se dodaje podmeni
        icon:putanja do ikonice koja ce biti postavljena za polje
        label:(string)tekst koji ce biti ispisan na podmeniju
        action: funkcionalnost podmenija
        status_message:(string)poruka koja ce biti ispisana na statusnoj liniji
        keyboard_shortcut:precica na tastaturi za funkcionalnost
    """
    def submenu(self,menu,icon,label,parent,action=None,status_message=None,keyboard_shortcut=None):
        #new_action=QAction(QIcon(icon),label,self,shortcut=keyboard_shortcut,statusTip=status_message,triggered=action)
        new_action=QAction(QIcon(icon),label,parent)
        menu.addAction(new_action)

        