'''
Created on Nov 15, 2014

@author: mladen
'''
from PySide.QtGui import QMainWindow, QTextEdit, QAction, QIcon

import Menu as m
import Toolbar as t
import file_handler as f


class window(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.textEdit=QTextEdit()
        self.menu=m.Menu(self)
        self.setMenuBar(self.menu)
        self.menu.submenu(self.fileMenu,'../../resources/images/New.png','&New',self)
        #menu.submenu(menu.fileMenu,'../../resources/images/New.png','&New',f.new_text_file(self),"dawda",'Ctrl+N')
        toolbar=t.Toolbar(self)
        toolbar.separator()
        self.setWindowTitle("Test Window")
        self.setGeometry(500,300,600,400)
        