'''
Created on Dec 6, 2014

@author: mladen
'''
from PySide.QtGui import QToolBar, QAction, QIcon


class Toolbar():
    '''
    Klasa koja olaksava kreiranje i dodavanje toolbarova.
    '''
    def __init__(self, parent):
        self.__parent=parent
        self.bar=self.__parent.addToolBar('Main')
    """Funkcija koja dodaje novu akciju na toolbar
    Args:
        icon:putanja do ikonice koja ce biti postavljena na toolbaru
        label:tekst koji ce biti ispisan za akciju
        action:funkcionalnost polja
        status_message:poruka koja ce biti ispisana na statusnoj liniji
        keyboard_shortcut:precica na tastaturi
    """
    def add(self,icon,label,action,status_message=None,keyboard_shortcut=None):
        new_action=QAction(QIcon(icon),label,self,shortcut=keyboard_shortcut,statusTip=status_message,triggered=action)
        self.bar.addAction(new_action)
    """Funkcija koja dodaje separator na toolbar"""
    def separator(self):
        self.bar.addSeparator()
        