'''
Created on Nov 15, 2014

@author: mladen
'''
import sys
from PySide.QtGui import QApplication
from classes import mainwindow, Menu

if __name__ == '__main__':
    app=QApplication(sys.argv)
    window=mainwindow.window()
    window.show()
    app.exec_()
    sys.exit(0)