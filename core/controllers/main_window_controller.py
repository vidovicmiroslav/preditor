'''
Created on Dec 7, 2014

@author: mladen
'''
from PySide.QtGui import QMessageBox


def editorInfo():
    '''
    Funkcija koja ispisuje osnovne informacije o Preditoru
    '''
    QMessageBox.about(None,"Information about Preditor", "Preditor is a generic editor,"
                          "capable of opening and editing various file types.")