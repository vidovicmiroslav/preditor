'''
Created on Dec 7, 2014

@author: mladen
'''

from PySide.QtGui import QMainWindow, QIcon, QStatusBar, QAction, QTextEdit, \
    QKeySequence, QFileDialog, QFontDialog, QFont, QMessageBox


class Text_Window(QMainWindow):
    '''
    Tekst editor, odnosno program za obradu teksta,
    '''
    

    def __init__(self):
        '''
        Konstruktor za editor teksta, postavlja sve komponente prozora.
        '''
        QMainWindow.__init__(self)
        self.setWindowTitle("TEXT PREDITOR")
        self.setWindowIcon(QIcon("resources/images/PREDITOR2.png"))
        self.textEdit=QTextEdit()
        self.fileName=None
        self.filters="Text files (*.txt)"
        self.setCentralWidget(self.textEdit)
        self.setGeometry(300,250,400,300)
       
        
    def setupComponents(self):
        '''
        Postavljanje komponenti prozora
        '''
        self.createMenus()
        self.createToolBar()
        self.createActions()
        self.addActions()
        self.myStatusBar=QStatusBar()
        self.setStatusBar(self.myStatusBar)
        self.myStatusBar.showMessage("Ready",10000)
        
    def createMenus(self):
        '''
        Postavljanje menija prozora.
        Glavni prozor ima samo plugin i about meni, jer on nema nikakvih dodatnih funkcionalnosti.
        '''
        self.fileMenu=self.menuBar().addMenu('&File')
        self.editMenu=self.menuBar().addMenu('&Edit')
        self.formatMenu=self.menuBar().addMenu('&Format')
        self.helpMenu=self.menuBar().addMenu('&Help')
        
    def addActions(self):
        '''
        Dodavanje akcija, odnosno podmenija na menije i toolbar
        '''
        self.fileMenu.addAction(self.newAction)
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.saveAction)
        self.editMenu.addAction(self.copyAction)
        self.editMenu.addAction(self.cutAction)
        self.editMenu.addAction(self.pasteAction)
        self.editMenu.addSeparator()
        self.editMenu.addAction(self.undoAction)
        self.editMenu.addAction(self.redoAction)
        self.formatMenu.addAction(self.fontAction)
        self.mainToolBar.addAction(self.newAction)
        self.mainToolBar.addAction(self.openAction)
        self.mainToolBar.addAction(self.saveAction)
        self.mainToolBar.addSeparator()
        self.mainToolBar.addAction(self.copyAction)
        self.mainToolBar.addAction(self.cutAction)
        self.mainToolBar.addAction(self.pasteAction)
        self.helpMenu.addAction(self.aboutAction)
        
        
        
    def createActions(self):
        '''
        Kreiranje novih akcija koje ce se dodavati na menije i toolbar
        '''
        self.newAction=QAction(QIcon('resources/images/New.png'),'&New',self,shortcut=QKeySequence.New,statusTip="Creating new file",
                               triggered=self.newFile)
        self.openAction=QAction(QIcon('resources/images/Open.png'),'&Open',self,shortcut=QKeySequence.Open,statusTip="Opening a file",
                               triggered=self.openFile)
        self.saveAction=QAction(QIcon('resources/images/Save.png'),'&Save',self,shortcut=QKeySequence.Save,statusTip="Saving file",
                               triggered=self.saveFile)
        self.cutAction=QAction(QIcon('resources/images/Cut.png'),'&Cut',self,shortcut=QKeySequence.Cut,statusTip="Cut",
                               triggered=self.textEdit.cut())
        self.copyAction=QAction(QIcon('resources/images/Copy.png'),'C&opy',self,shortcut=QKeySequence.Copy,statusTip="Copy",
                               triggered=self.textEdit.copy())
        self.pasteAction=QAction(QIcon('resources/images/Paste.png'),'&Paste',self,shortcut=QKeySequence.Paste,statusTip="Paste",
                               triggered=self.textEdit.paste())
        self.undoAction=QAction(QIcon('resources/images/Undo.png'),'&Undo',self,shortcut=QKeySequence.Undo,statusTip="Undo",
                               triggered=self.textEdit.undo())
        self.redoAction=QAction(QIcon('resources/images/Redo.png'),'&Redo',self,shortcut=QKeySequence.Redo,statusTip="Redo",
                               triggered=self.textEdit.redo())
        self.fontAction=QAction(QIcon('resources/images/Globe.png'),'&Fonts',self,triggered=self.fontChange)
        self.aboutAction=QAction(QIcon('resources/images/Information.png'),'&About',self,triggered=self.editorInfo())
    def createToolBar(self):
        '''
        Kreiranje toolbara
        '''
        self.mainToolBar = self.addToolBar('Main')
        
        
        
#------------prebaciti file_handler 
    def newFile(self):
        self.textEdit.setText('')
        self.fileName = None
        
    def openFile(self):
        self.fileName, self.filterName = QFileDialog.getOpenFileName(self)
        self.textEdit.setText(open(self.fileName).read())
        
    def saveFile(self):
        if self.fileName == None or self.fileName =='':
            self.fileName, self.filterName = QFileDialog.getSaveFileName(self, filter = self.filters)
        if (self.fileName != ''):
            file = open(self.fileName, 'w')
            file.write(self.textEdit.toPlainText())
        self.statusBar().showMessage("Fajl sacuvan", 2000)
        
    def fontChange(self):
        (font,ok)=QFontDialog.getFont(QFont("Helvetica [Cronyx]",10),self)
        if ok:
            self.textEdit.setCurrentFont(font)
    
    def editorInfo(self):
        QMessageBox.about(self,"Information about Preditor", "Preditor is a generic editor,"
                          "capable of opening and editing various file types.")
        
        