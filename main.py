'''
Created on Nov 11, 2014

@author: mladen
'''
from PySide.QtGui import QApplication
import sys

from core.views import main_window


if __name__ == '__main__':
    app=QApplication(sys.argv)
    window=main_window.Text_Window()
    window.setupComponents()
    window.show()
    app.exec_()
    sys.exit(0)