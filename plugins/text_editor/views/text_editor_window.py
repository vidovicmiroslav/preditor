'''
Created on Dec 7, 2014

@author: mladen
'''

from PySide.QtGui import QMainWindow, QIcon, QStatusBar, QAction, QTextEdit, \
    QKeySequence, QFileDialog

from plugins.text_editor.controllers import file_handler as ctr


class Text_Window(QMainWindow):
    '''
    Tekst editor, odnosno program za obradu teksta,
    '''
    

    def __init__(self):
        '''
        Konstruktor za editor teksta, postavlja sve komponente prozora.
        '''
        QMainWindow.__init__(self)
        self.setWindowTitle("TEXT PREDITOR")
        self.setWindowIcon(QIcon("../../resources/images/PREDITOR2.png"))
        self.textEdit=QTextEdit()
        self.fileName=None
        self.setCentralWidget(self.textEdit)
        self.setGeometry(300,250,400,300)
        self.setupComponents()
        self.createMenus()
        self.createToolBar()
        self.createActions()
        self.addActions()
        
    def setupComponents(self):
        '''
        Postavljanje komponenti prozora
        '''
        self.myStatusBar=QStatusBar()
        self.setStatusBar(self.myStatusBar)
        self.myStatusBar.showMessage("Ready",10000)
        
    def createMenus(self):
        '''
        Postavljanje menija prozora.
        Glavni prozor ima samo plugin i about meni, jer on nema nikakvih dodatnih funkcionalnosti.
        '''
        self.fileMenu=self.menuBar().addMenu('&File')
        self.editMenu=self.menuBar().addMenu('&Edit')
        self.formatMenu=self.menuBar().addMenu('&Format')
        
    def addActions(self):
        '''
        Dodavanje akcija, odnosno podmenija na menije i toolbar
        '''
        self.fileMenu.addAction(self.newAction)
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.saveAction)
        self.mainToolBar.addAction(self.newAction)
        self.mainToolBar.addAction(self.openAction)
        self.mainToolBar.addAction(self.saveAction)
        
        
        
    def createActions(self):
        '''
        Kreiranje novih akcija koje ce se dodavati na menije i toolbar
        '''
        self.newAction=QAction(QIcon('../../resources/images/New.png'),'&New',self,shortcut=QKeySequence.New,statusTip="Creating new file",
                               triggered=self.newFile)
        self.openAction=QAction(QIcon('../../resources/imagesOpen.png'),'&Open',self,shortcut=QKeySequence.Open,statusTip="Opening a file",
                               triggered=self.openFile)
        self.saveAction=QAction(QIcon('../../resources/images/Save.png'),'&Save',self,shortcut=QKeySequence.Save,statusTip="Saving file",
                               triggered=self.saveFile)
        
    def createToolBar(self):
        '''
        Kreiranje toolbara
        '''
        self.mainToolBar = self.addToolBar('Main')
        
        
        
#------------prebaciti file_handler 
    def newFile(self):
        self.textEdit.setText('')
        self.fileName = None
        
    def openFile(self):
        self.fileName, self.filterName = QFileDialog.getOpenFileName(self)
        self.textEdit.setText(open(self.fileName).read())
        
    def saveFile(self):
        if self.fileName == None or self.fileName =='':
            self.fileName, self.filterName = QFileDialog.getSaveFileName(self, filter = self.filters)
        if (self.fileName != ''):
            file = open(self.fileName, 'w')
            file.write(self.textEdit.toPlainText())
        self.statusBar().showMessage("Fajl sacuvan", 2000)
        
        
        