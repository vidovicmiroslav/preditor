'''
Created on Dec 7, 2014

@author: mladen
'''
from PySide.QtGui import QApplication
import sys
from views import text_editor_window as window

if __name__=='__main__':
    app=QApplication(sys.argv)
    editor=window.Text_Window()
    editor.show()
    app.exec_()
    sys.exit(0)
    