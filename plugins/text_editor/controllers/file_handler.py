'''
Created on Dec 7, 2014

@author: mladen
'''
from PySide.QtGui import QFileDialog, QMainWindow




    
        
def openFile(self):
    self.fileName, self.filterName = QFileDialog.getOpenFileName(self)
    self.textEdit.setText(open(self.fileName).read())
        
def saveFile(self):
    if self.fileName == None or self.fileName =='':
        self.fileName, self.filterName = QFileDialog.getSaveFileName(self, filter = self.filters)
    if (self.fileName != ''):
        file = open(self.fileName, 'w')
        file.write(self.textEdit.toPlainText())
        self.statusBar().showMessage("Fajl sacuvan", 2000)